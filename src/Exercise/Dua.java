package Exercise;

import java.util.Scanner;

public class Dua {

    private static Scanner input = new Scanner(System.in);

    private static boolean flag = true;

    public static void cekPin (){
        System.out.println("Login untuk melakukan Transaksi");
        int countSalah = 0;
        flag = true;
        while (flag) {
            if (countSalah == 3){
                System.out.println("Akun anda di blokir!");
                flag = false;
                break;
            }
            System.out.println("Masukkan PIN : ");
            String PIN = input.nextLine();
            if (PIN.equals(Exercise.PIN.getPIN())) {
                flag = false;
            }
            if (!PIN.equals(Exercise.PIN.getPIN())) {
                System.out.println("Password salah, mohon ulangi");
                countSalah++;
            }
        }
    }
}
