package Exercise;

import java.util.Scanner;

public class PIN {

    private static String Pin;
    private static String Pin2;
    private static Scanner input = new Scanner(System.in);

    public static void buatPIN(){
        boolean flag = true;
        while (flag) {
            System.out.println("Masukkan pin : (6 digit)");
            System.out.println("Pastikan hanya terdiri dari angka");
            System.out.print("Masukkan : ");
            Pin = input.nextLine();

            if (Pin.length() !=6) {
                System.out.println("Angka tidak sesuai, harus 6 digit");
            } else {
                if (utility.IsNumeric(Pin)){
                    flag = false;
                }
            }
        }
    }
    public static String getPIN() {
        return Pin;
    }
}