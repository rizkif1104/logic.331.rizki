package PreTest;

import java.util.Scanner;

public class MenghitungPorsi05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int jumlahOrang;
        double totalPorsi = 0;
        double totalPorsiSemua = 0;
        boolean flag = true;

        while (flag){
            System.out.println("Masukkan jenis orang dan jumlahnya");
            System.out.println("1. Orang dewasa Laki-Laki");
            System.out.println("2. Orang dewasa Perempuan");
            System.out.println("3. Orang Remaja");
            System.out.println("4. Anak anak");
            System.out.println("5. Balita");
            System.out.println("6. Hitung Total");

            int jenisPorsi = input.nextInt();

            while (jenisPorsi < 1 || jenisPorsi > 6)
            {
                System.out.println("pilihan tidak tersedia");
                jenisPorsi = input.nextInt();
            }

            switch (jenisPorsi)
            {
                case 1:
                    System.out.println("Masukkan Jumlah Orang");
                    jumlahOrang = input.nextInt();
                    totalPorsi = jumlahOrang * 2;
                    totalPorsiSemua = totalPorsiSemua + totalPorsi;
                    break;
                case 2:
                    System.out.println("Masukkan jumlah Orang");
                    jumlahOrang = input.nextInt();
                    totalPorsi = jumlahOrang * 1;
                    totalPorsiSemua = totalPorsiSemua + totalPorsi;
                    break;
                case 3:
                    System.out.println("Masukkan jumlah Orang");
                    jumlahOrang = input.nextInt();
                    totalPorsi = jumlahOrang * 1;
                    totalPorsiSemua = totalPorsiSemua + totalPorsi;
                    break;
                case 4:
                    System.out.println("Masukkan jumlah Orang");
                    jumlahOrang = input.nextInt();
                    totalPorsi = jumlahOrang * 0.5;
                    totalPorsiSemua = totalPorsiSemua + totalPorsi;
                    break;
                case 5:
                    System.out.println("Masukkan jumlah Orang");
                    jumlahOrang = input.nextInt();
                    totalPorsi = jumlahOrang * 1;
                    totalPorsiSemua = totalPorsiSemua + totalPorsi;
                    break;

                case 6:
                    flag = false;
                    if (totalPorsiSemua > 5){
                        totalPorsiSemua = totalPorsiSemua + 1;
                    }
                    System.out.println("Total Porsi = " + totalPorsiSemua);
                    break;
            }
        }
    }
}
