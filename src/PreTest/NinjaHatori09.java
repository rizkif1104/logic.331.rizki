package PreTest;

import java.util.Scanner;

public class NinjaHatori09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan rute hatorri");
        System.out.println("Contoh : N N T T");
        String rute = input.nextLine();

        String ruteSplit = rute.trim().toLowerCase();

        char[] ruteArray = ruteSplit.toCharArray();

        int ketinggian = 0;
        int tambah = 0;
        int kurang = 0;

        int countGunung = 0;
        int countLembah = 0;

        for (int i = 0; i < ruteArray.length; i++) {
            if (ruteArray[i] == 'n')
            {
                tambah ++;
            }
            else if (ruteArray[i] == 't')
            {
                kurang ++;
            }
            ketinggian = tambah - kurang;

            if (ketinggian == 0 && ruteArray[i] == 't'){
                countGunung++;
            }
            else if (ketinggian == 0 && ruteArray[i] == 'n') {
                countLembah++;
            }
        }
        System.out.println("jumlah gunung = " + countGunung);
        System.out.println("jumlah lembah = " + countLembah);
    }
}
