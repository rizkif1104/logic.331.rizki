package PreTest;

import java.util.Scanner;

public class PengantarMakanan04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Rute");
        System.out.println("1. Tempat 1");
        System.out.println("2. Tempat 2");
        System.out.println("3. Tempat 3");
        System.out.println("4. Tempat 4");
        System.out.println("Contoh = 1-2-3-4");
        System.out.println("Masukkan tempat yang ingin dikunjungi ");
        String inputRute = input.nextLine();

        String[] splitRute = inputRute.split("-");
        int[] total = new int[splitRute.length];

        for (int i = 0; i < splitRute.length; i++) {
            total[i] = Integer.parseInt(splitRute[i]);
        }

        double[] jarakToko = {0,2,0.5,1.5,2.5};

        int posisiAwal = 0;
        double jarakPindah = 0;
        double posisiSebelunya = 0;
        double totalJarak = 0;

        for (int i = 0; i < total.length; i++) {
            posisiAwal = total[i];
            jarakPindah = jarakToko[posisiAwal] + posisiSebelunya;
            posisiSebelunya = jarakToko[posisiAwal];
            totalJarak += Math.abs(jarakPindah);
        }

        double jarakPulang = jarakToko[posisiAwal] + totalJarak;
        double bensin = 0.4;
        double bensinTotal = (jarakPulang * bensin);

        System.out.println("bensin yang dihabiskan = " + bensinTotal + " liter");

    }
}
