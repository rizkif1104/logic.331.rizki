package PreTest;

import java.util.Scanner;

public class DeretAngkaPenjumlahan08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("input nilai n ");
        int n = input.nextInt();

        int hitung = 0;
        int counter = 0;
        int jumlahData = 0;
        int helper = 0;
        int helper1 = 1;
        int[] arrayPrime = new int[n];
        int[] arrayFibonanci = new int[n];
        int[] arrayTotal = new int[n];

        //prima
        for (int i = 0; i < n * 10; i++) {
            for (int j = 1; j <= i ; j++) {
                if (i % j == 0)
                {
                    hitung++;
                }
            }
            if (hitung == 2){
                arrayPrime[counter] = i;
                counter++;
                jumlahData++;
            }
            if (jumlahData == n)
            {
                break;
            }
            hitung = 0;
        }
        System.out.print("Bilangan prima = ");
        for (int i = 0; i < n; i++) {
            System.out.print(arrayPrime[i] + ", ");
        }
        System.out.println();

        //fibonanci
        for (int i = 0; i < n; i++) {
            if (i == 0)
            {
                arrayFibonanci[i] = helper1;
            }
            else
            {
                arrayFibonanci[i] = helper + helper1;
                helper = helper1;
                helper1 = arrayFibonanci[i];
            }
        }
        System.out.print("Bilangan Fibonanci = ");
        for (int i = 0; i < n; i++) {
            System.out.print(arrayFibonanci[i] + ", ");
        }
        System.out.println();

        //total
        System.out.print("Total = ");
        for (int i = 0; i < n; i++) {
            arrayTotal[i] = arrayPrime[i] + arrayFibonanci[i];
            System.out.print(arrayTotal[i] + ", ");
        }
    }
}
