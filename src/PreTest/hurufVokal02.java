package PreTest;

import java.util.Scanner;

public class hurufVokal02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kalimat");
        String kalimat = input.nextLine();
        char[] kalimatChar = kalimat.toLowerCase().toCharArray();

        char[] hurufVokal = ("aiueo").toCharArray();
        char[] hurufMati = ("bcdfghjklmnpqrstvwxyz").toCharArray();

        for (int i = 0; i < hurufVokal.length; i++) {
            for (int j = 0; j < kalimatChar.length; j++) {
                char huruf = kalimatChar[j];
                if (huruf == hurufVokal[i])
                {
                    System.out.print(kalimatChar[j]);
                }
            }
        }

        System.out.println();

        for (int i = 0; i < hurufMati.length; i++) {
            for (int j = 0; j < kalimatChar.length; j++) {
                char huruf = kalimatChar[j];
                if (huruf == hurufMati[i])
                {
                    System.out.print(kalimatChar[j]);
                }
            }
        }
    }
}
