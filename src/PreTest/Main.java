package PreTest;

import FinalPractice.StikEsLoli01;
import FinalPractice.formatJam05;
import FinalPractice.takaranCupcake10;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                GanjilGenap01.Resolve(); //done
                break;
            case 2:
                hurufVokal02.Resolve(); //done
                break;
            case 3:
                AkuSiAngka03.Resolve(); //done
                break;
            case 4:
                PengantarMakanan04.Resolve(); //done
                break;
            case  5:
                MenghitungPorsi05.Resolve(); //done
                break;
            case 6:
                TransferBank06.Resolve(); //done
                break;
            case 7:
                mainKartu07.Resolve();
                break;
            case 8:
                DeretAngkaPenjumlahan08.Resolve(); //done
                break;
            case 9:
                NinjaHatori09.Resolve(); //done
                break;
            case 10:
                Kopi80Ribu10.Resolve(); //done
                break;
            default:
        }
    }
}