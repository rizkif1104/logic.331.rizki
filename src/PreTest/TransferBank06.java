package PreTest;

import java.util.Scanner;

public class TransferBank06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int saldo = 0;
        int saldoTotal;
        int setorTunai;
        int admin = 7500;
        boolean flag = true;

        while (flag){
            System.out.print("Masukkan pin ");
            int pin = input.nextInt();
            if (pin == 123456)
            while (flag){
                System.out.println("Main Menu");
                System.out.println("1. Setor tunai");
                System.out.println("2. Transfer");
                System.out.println("3. Exit");
                int pilihan = input.nextInt();

                while (pilihan < 1 || pilihan > 3){
                    System.out.println("pilihan tidak tersedia, kembali ke awal");
                    pilihan = input.nextInt();
                }

                switch (pilihan)
                {
                    case 1:
                        System.out.println("Masukkan jumlah saldo");
                        setorTunai = input.nextInt();
                        saldo = saldo + setorTunai;
                        System.out.println("saldo berhasil anda masukkan");
                        System.out.println("Saldo anda = " + saldo);
                        break;
                    case 2:
                        System.out.println("1. Transfer antar rekening");
                        System.out.println("2. Transfer antar bank");
                        int tipeTransfer = input.nextInt();

                        if (tipeTransfer == 1) {
                            System.out.println("Masukkan Nomor Rekening");
                            int noRekening = input.nextInt();
                            System.out.println("Masukkan jumlah saldo yang ingin di transfer");
                            int saldoTransfer = input.nextInt();
                            saldoTotal = saldo - saldoTransfer;
                            if (saldoTransfer > saldo)
                            {
                                System.out.println("saldo anda tidak cukup silahkan masukkan saldo lagi");
                                saldoTransfer = input.nextInt();
                            }
                            else
                            {
                                System.out.println("Transfer berhasil");
                                System.out.println("jumlah saldo anda = " + saldoTotal);
                            }
                        } else if (tipeTransfer == 2) {
                            System.out.println("Masukkan kode bank ");
                            int kodeBank = input.nextInt();
                            System.out.println("Masukkan Nomor Rekening");
                            int noRekening = input.nextInt();
                            System.out.println("Masukkan jumlah saldo yang ingin di transfer");
                            int saldoTransfer = input.nextInt();
                            saldoTotal = saldo - saldoTransfer - admin;
                            if (saldoTransfer > saldo)
                            {
                                System.out.println("Saldo anda tidak cukup silahkan masukkan saldo lagi");
                                saldoTransfer = input.nextInt();
                            }
                            else {
                                System.out.println("Transfer Berhasil");
                                System.out.println("Jumlah saldo anda = " + saldoTotal);
                            }
                        }
                        break;
                    case 3:
                        flag = false;
                        break;
                }
            }
        }
    }
}
