package PreTest;

import java.util.Scanner;

public class GanjilGenap01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("input n :");
        int n = input.nextInt();
        int ganjil = 1;
        int genap = 2;

        int[] array1d = new int[n];

        while (ganjil <= n){
            System.out.print(ganjil + " ");
            ganjil += 2;
        }
        System.out.println();

        while (genap <= n){
            System.out.print(genap + " ");
            genap += 2;
        }
        System.out.println();
    }
}
