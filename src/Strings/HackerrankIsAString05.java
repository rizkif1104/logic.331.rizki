package Strings;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HackerrankIsAString05 {
        public static boolean hackerRank(String s) {
            // cek apakah data string mengandung karakter 'hackerrank'
            Pattern p = Pattern.compile(".*h.*a.*c.*k.*e.*r.*r.*a.*n.*k.*");
            Matcher m = p.matcher(s);

            return m.matches();
        }

        public static void Resolve() {
            Scanner in = new Scanner(System.in);
            System.out.println("masukkan jumlah kalimat : ");
            int jumlahKalimat = in.nextInt();
            for(int a0 = 0; a0 < jumlahKalimat; a0++){
                System.out.println("Masukkan kalimat : ");
                String kalimat = in.next();
                System.out.println((hackerRank(kalimat)) ? "YES" : "NO");
            }
            in.close();
        }
}