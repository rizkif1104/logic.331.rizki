package Strings;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (0 - 10)");
        pilihan = input.nextInt();

        while (pilihan < 0 || pilihan > 10)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 0:
                coba.Resolve();
                break;
            case 1:
                CamelCase01.Resolve(); //done
                break;
            case 2:
                StrongPassword02.Resolve(n); //done
                break;
            case 3:
                CaesarCipher03.Resolve(); //done
                break;
            case 4:
                // Soalno4.Resolve(); //error
                break;
            case 5:
                HackerrankIsAString05.Resolve(); //done
                break;
            case 6:
                Pangrams06.Resolve(n); //done
                break;
            case 7:

                break;
            case 8:

                break;
            case 9:
                MakingAnagrams09.Resolve(); //done
                break;
            case 10:
                TwoStrings10.Resolve(); //done
                break;
            default:
        }
    }
}