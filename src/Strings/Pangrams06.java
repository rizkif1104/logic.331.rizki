package Strings;

import java.util.Scanner;

public class Pangrams06 {
    public static void Resolve(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input kalimat :");
        String kata = input.nextLine();

        boolean[] alfabet = new boolean[26];

        kata = kata.toLowerCase();

        for (int i = 0; i < kata.length(); i++) {
            char c = kata.charAt(i);
            if (c >= 'a' && c <= 'z') {
                int index = c - 'a';
                alfabet[index] = true;
            }
        }

        boolean cek = true;
        for (boolean letter : alfabet) {
            if (!letter) {
                cek = false;
                break;
            }
        }

        if (cek) {
            System.out.println("pangram");
        } else {
            System.out.println("not pangram");
        }
    }
}