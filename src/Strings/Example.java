package Strings;

import java.util.Scanner;

public class Example {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan kalimat : ");
        String kalimat = input.nextLine();
        String [] kalimatBintang = kalimat.split(" ");

        for (int i = 0; i < kalimatBintang.length; i++) {
            char[] huruf = kalimatBintang[i].toCharArray();
            for (int j = 0; j < huruf.length; j++) {
                if (j == 0 || j == huruf.length - 1)
                {
                    System.out.print(huruf[j]);
                }
                else
                {
                    System.out.print("*");
                }
            }
            System.out.print(" ");
        }
    }
}
