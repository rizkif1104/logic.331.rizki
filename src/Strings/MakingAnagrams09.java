package Strings;

import java.util.Scanner;

public class MakingAnagrams09 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan huruf = ");
        String baris1 = input.nextLine();
        String baris2 = input.nextLine();

        int jumlah = 0;
        int semuaData = baris1.length() + baris2.length();
        int dataTidakSama = 0;

        for (int i = 0; i < baris1.length(); i++) {
            if (baris2.contains(baris1.substring(i,i + 1))){
                jumlah++;
            }

        }
        dataTidakSama = semuaData - (jumlah * 2);
        System.out.println(dataTidakSama);
    }
}