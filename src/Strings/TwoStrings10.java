package Strings;

import java.util.*; //gabungan java util scanner sama java util hashset

public class TwoStrings10 {
    static  Set<Character> a;
    static  Set<Character> b;

    public static void Resolve(){
        Scanner scan = new Scanner(System.in);
        System.out.println("masukkan jumlah kalimat : ");
        int n = scan.nextInt();
        for (int i = 0; i < n; i++) {
            a = new HashSet<Character>();
            b = new HashSet<Character>();

            for (char c : scan.next().toCharArray()) {
                a.add(c);
            }
            for (char c : scan.next().toCharArray()) {
                b.add(c);
            }
            a.retainAll(b);
            System.out.println((a.isEmpty()) ? "NO" : "YES");
        }
        scan.close();
    }
}