package Strings;

public class MarsExploration04 {
    public static int Resolve(String s) {
        int cnt = 0;
        int i = 0;
        String temp = "";

        while (i < s. length() - 1){
            temp = s. substring(i,i + 3);
            if (temp.charAt(0)!='S') cnt++ ;
            if (temp.charAt(1)!='O') cnt++ ;
            if (temp.charAt(2)!='S') cnt++ ;
            i+=3;
        }
        return cnt;
    }
}