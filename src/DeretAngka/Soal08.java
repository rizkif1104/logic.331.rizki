package DeretAngka;

public class Soal08 {
    public static void Resolve(int n){
        int helper = 3;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {
            array1d[i] = helper;
            helper = helper * 3;
        }
        Utility.PrintArray1D(array1d);
    }
}
