package DeretAngka;

public class Soal05 {
    public static void Resolve(int n){
        int helper = 1;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {

            if (i % 3 == 2)
            {
                System.out.print("* ");
            }
            else {
                array1d[i] = helper;
                helper = helper + 4;
                System.out.print(array1d[i] + " ");
            }
        }
    }
}
