package DeretAngka;

public class Soal07 {
    public static void Resolve(int n){
        int helper = 2;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {
            array1d[i] = helper;
            helper = helper * 2;
        }
        Utility.PrintArray1D(array1d);
    }
}
