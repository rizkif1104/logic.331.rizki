package DeretAngka;

public class Soal09 {
    public static void Resolve(int n){
        int helper = 4;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {
            array1d[i] = helper;
            if (i % 3 == 2){
                System.out.print("* ");
            } else {
                System.out.print(array1d[i] + " ");
                helper = helper * 4;
            }
        }
    }
}
