package DeretAngka;

public class Soal06 {
    public static void Resolve(int n){
        int helper = 1;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {
            array1d[i] = helper;
            if (i % 3 == 2){
                System.out.print("* ");
                helper = helper + 4;
            } else  {
                System.out.print(array1d[i] + " ");
                helper = helper + 4;
            }
        }
    }
}
