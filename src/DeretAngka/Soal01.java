package DeretAngka;

public class Soal01 {
    public static void Resolve(int n)
    {
        int helper = 1;
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = helper;
            helper += 2;
        }
        Utility.PrintArray1D(array);
    }
}
