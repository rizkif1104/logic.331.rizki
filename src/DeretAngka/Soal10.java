package DeretAngka;

public class Soal10 {

    public static void Resolve(int n){
        int helper = 3;
        int[] array1d = new int[n];

        for (int i = 0; i < n; i++) {
            array1d[i] = helper;
            if (i % 4 == 3){
                System.out.print("XXX ");
                helper = helper * 3;
            } else {
                System.out.print(array1d[i] + " ");
                array1d[i] = helper;
                helper = helper * 3;
            }
        }
    }
}