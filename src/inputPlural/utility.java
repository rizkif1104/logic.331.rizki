package inputPlural;

public class utility {
    public static int[] ConvertStringArrayInt(String text)
    {
        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        return intArray;
    }

    public static boolean isNumber(String text){
        try {
            int angka = Integer.parseInt(text.replace(" ",""));
            return true;
        }catch (NumberFormatException e){
            System.out.println("Pastikan hanya tipe nomor saja!");
            return false;
        }
    }

    public static String[] ConvertStringToArrayString(String text){
        String[] textArray = text.split(" ");
        String[] stringArray = new String[textArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = String.valueOf(textArray[i]);
        }

        return stringArray;
    }
}
