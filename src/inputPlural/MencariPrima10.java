package inputPlural;

import java.util.Scanner;

public class MencariPrima10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input derat angka");
        String valueInput = input.nextLine();

        int[] value = utility.ConvertStringArrayInt(valueInput);
        int count = 0;
        int panjang = value.length;
        String prime = "";

        for (int i = 0; i < panjang; i++) {
            for (int j = 0; j < value[i] + 1; j++) {
                if (value[i] % (j+1) == 0){
                    count++;
                }
            }
            if (count == 2){
                prime += value[i] + " ";
            }
            count = 0;
        }

        System.out.println("Output : " + prime + "adalah bilangan prima");
    }
}