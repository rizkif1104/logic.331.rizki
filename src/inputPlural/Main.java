package inputPlural;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (0 - 12)");
        pilihan = input.nextInt();

        while (pilihan < 0 || pilihan > 12)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 0:
                example.Resolve();
                break;
            case 1:
                PenjumlahanInput01.Resolve(); //done
                break;
            case 2:
                RataRata02.Resolve(); //done
                break;
            case 3:
                Median03.Resolve(); //done
                break;
            case 4:
                Modus04.Resolve(); //done
                break;
            case 5:
                UrutkanDeret05.Resolve(); //done
                break;
            case 6:
                HitungPasangan06.Resolve(); //done
                break;
            case 7:
                TerbesarTerkecil07.Resolve(); //done
                break;
            case 8:
                BanyakAngkaTerbesar08.Resolve(); //done
                break;
            case 9:
                Presentase09.Resolve(); //done
                break;
            case 10:
                MencariPrima10.Resolve(); //done
                break;
            case 11:
                AscendingDescending11.Resolve(); //done
                break;
            case 12:
                Ranking12.Resolve(); //not done
                break;
            default:
        }
    }
}