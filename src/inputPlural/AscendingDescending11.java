package inputPlural;

import java.util.Scanner;

public class AscendingDescending11 {

    private final static String[] Alfabet =
            {"a","b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
                    "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    private static String Ascending(String[] value){
        String nilaiUrut = "";
        int panjangChara = Alfabet.length;
        int panjangInput = value.length;
        int count = 0;

        for (int i = 0; i < panjangChara; i++) {
            for (int j = 0; j < panjangInput; j++) {
                if (Alfabet[i].equals(value[j])){
                    nilaiUrut += Alfabet[i] + " ";
                    count ++;
                }
            }
        }
        return nilaiUrut;
    }

    private static String Descending(String[] value){
        String nilaiUrut = "";
        int panjangChara = Alfabet.length;
        int panjangInput = value.length;
        int count = 0;

        for (int i = 0; i < panjangChara; i++) {
            for (int j = 0; j < panjangInput; j++) {
                if (Alfabet[i].equals(value[j])){
                    nilaiUrut += Alfabet[panjangChara - (i+1)] + " ";
                    count ++;
                }
            }
            if (count == panjangInput){
                break;
            }
        }
        return nilaiUrut;
    }

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input derat alfabet");
        String valueInput = input.nextLine();
        String[] value = utility.ConvertStringToArrayString(valueInput.toLowerCase());
        int panjang = value.length;
        String output = "";
        boolean flag = true;

        while (flag){
            System.out.println("1. Asc");
            System.out.println("2. Dsc");
            String valueInput2 = input.nextLine();
            if (valueInput2.length() < 2 && !valueInput2.isEmpty()){
                if (utility.isNumber(valueInput2)){
                    if (valueInput2.equals("1")){
                        output = Ascending(value);
                    }else if (valueInput2.equals("2")){
                        output = Descending(value);
                    }
                    flag = false;
                }
            }
        }
        System.out.println("output : " + output);
    }
}
