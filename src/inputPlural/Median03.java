package inputPlural;

import java.util.Scanner;

public class Median03 {
    public static void Resolve() {
        boolean flag = true;
        Scanner input = new Scanner(System.in);
        while (flag){
            System.out.println("Input: 9\t2\t3\t4\t5\t7\t6\t1");
            System.out.println("Input derat angka");
            String angka = input.nextLine();

            if (utility.isNumber(angka)){
                int[] value = utility.ConvertStringArrayInt(angka);
                int panjang = value.length;
                int tengah = panjang/2;

                boolean genap = panjang % 2 == 0 ? true : false;
                double median;

                if (genap){
                    median = (double) (value[tengah-1] + value[tengah]) / 2;
                } else {
                    median = value[tengah];
                }

                System.out.println("Nilai tengah = " + median);
                flag = false;
            }
        }
    }
}
