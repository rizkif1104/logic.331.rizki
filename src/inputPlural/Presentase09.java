package inputPlural;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Presentase09 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        double positive = 0;
        double negative = 0;
        double zero = 0;

        System.out.println("Input derat angka");
        String valueInput = input.nextLine();

        int[] value = utility.ConvertStringArrayInt(valueInput);
        int panjang = value.length;

        for (int i = 0; i < panjang; i++) {
            if (value[i] < 0){
                negative++;
            }
            if (value[i] > 0){
                positive++;
            }
            if (value[i] == 0){
                zero++;
            }
        }
        DecimalFormat df = new DecimalFormat("0.0");
        positive = (positive/panjang) * 100;
        negative = (negative/panjang) * 100;
        zero = (zero/panjang) * 100;

        System.out.println("Positif : " + df.format(positive) + "%");
        System.out.println("Negatif : " +  df.format(negative) + "%");
        System.out.println("Zero : " + df.format(zero) + "%");
    }
}
