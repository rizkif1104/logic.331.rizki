package inputPlural;

import java.util.Scanner;

public class UrutkanDeret05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka : ");
        String inputan = input.nextLine();

        int[] nilai = utility.ConvertStringArrayInt(inputan);
        int panjang = nilai.length;
        int count = 0;
        int[] urutan = new int[panjang];

        for (int i = 0; i < panjang; i++) {
            for (int j = 0; j < panjang; j++) {
                if (nilai[i] != nilai[j]){
                    if (nilai[i] > nilai[j]){
                        count++;
                    }
                }
            }
            urutan[i] = count;
            count = 0;
        }
        for (int i = 0; i < panjang; i++) {
            for (int j = 0; j < panjang; j++) {
                if (i == urutan[j]){
                    System.out.print(nilai[j] + " ");
                }
            }
        }
    }
}
