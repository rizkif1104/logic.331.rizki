package inputPlural;

import java.util.Scanner;

public class Modus04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        boolean flag = true;
    while (flag){
        System.out.println("Input derat angka");
        String angka = input.nextLine();
        if (utility.isNumber(angka)){
            int[] value = utility.ConvertStringArrayInt(angka);
            int panjang = value.length;
            int hitungan = 0;
            int terbanyak = 0;
            int wadah = 0;

            int[] banyakNilai = new int[panjang];

            for (int i = 0; i < panjang; i++) {
                if (i == 0){
                    wadah = value[i];
                    for (int j = 0; j < panjang; j++) {
                        if (wadah == value[j]){
                            hitungan ++;
                        }
                    }
                    banyakNilai[i] = hitungan;
                    terbanyak = wadah;
                } else {
                    boolean isSame = false;
                    for (int j = 0; j < i; j++) {
                        if (value[i] == value[j]){
                            isSame = true;
                        }
                    }
                    if (!isSame){
                        for (int k = 0; k < panjang; k++) {
                            if (value[i] == value[k]){
                                hitungan ++;
                            }
                        }
                        banyakNilai[i] = hitungan;
                    }
                    else {
                        banyakNilai[i] = 0;
                    }
                }
                hitungan = 0;
            }
            int modus = 0;
            String output = "";
            for (int i = 0; i < panjang; i++) {
                if (modus == banyakNilai[i]){
                    output += value[i] + " ";
                }
                if (modus < banyakNilai[i]){
                    modus = banyakNilai[i];
                    output = value[i] + " ";
                }
            }
            System.out.println();
            System.out.println("Data sebanyak : " + output);
            flag = false;
            }
        }
    }
}