package inputPlural;

import java.util.Scanner;

public class BanyakAngkaTerbesar08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka = ");
        String inputNilai = input.nextLine();

        int[] nilai = utility.ConvertStringArrayInt(inputNilai);
        int panjang = nilai.length;
        int nilaiBesar = 0;
        int banyakTerbesar = 0;

        for (int i = 0; i < panjang; i++) {
            if (nilaiBesar < nilai[i]){
                nilaiBesar = nilai[i];
            }
        }
        for (int i = 0; i < panjang; i++) {
            if (nilaiBesar == nilai[i]){
                banyakTerbesar ++;
            }
        }
        System.out.print("Angka terbesar yang keluar = " + banyakTerbesar);
    }
}
