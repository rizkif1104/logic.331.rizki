package ProblemSolving14;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 14)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 14)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                DeretAngka01.Resolve(); //done
                break;
            case 2:
                BambangKaryawanGrosir02.Resolve(); //done
                break;
            case 3:
                RecordPenjualanBuah03.Resolve(); //done
                break;
            case 4:
                WishlistAndi04.Resolve(); //done
                break;
            case  5:
                FormatJam05.Resolve(); //done
                break;
            case 6:
                //Soal6.Resolve(); //not done
                break;
            case 7:
                LintasanJim07.Resolve(); //not done
                break;
            case 8:
                UangAndi08.Resolve(); //done
                break;
            case 9:
                TarifParkir09.Resolve(); //done
                break;
            case 10:
                KonversiBotol10.Resolve(); //done
                break;
            case 11:
                Belipulsa11.Resolve(); //done
                break;
            case 12:
                PenjualanBuah12.Resolve(); //done
                break;
            case 13:
                AlfabetNomer13.Resolve(); //done
                break;
            case 14:
                //Soalno14.Resolve(); //not done
                break;
            default:
        }
    }
}