package ProblemSolving14;

import java.util.Scanner;

public class DeretAngka01 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan nilai : ");
        int n = input.nextInt();

        int[] baris1 = new int[n];
        int[] baris2 = new int[n];
        int[] baris3 = new int[n];

        for (int i = 0; i < n; i++){
            baris1[i] = (i * 3) - 1;
            System.out.print(baris1[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < n; i++) {
            baris2[i] = (i * (-2)) * 1;
            System.out.print(baris2[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < n; i++) {
            baris3[i] = baris1[i] + baris2[i];
            System.out.print(baris3[i] + " ");
        }
    }
}
