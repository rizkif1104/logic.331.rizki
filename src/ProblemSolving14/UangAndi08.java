package ProblemSolving14;

import java.util.Arrays;
import java.util.Scanner;

public class UangAndi08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Uang andi : 70");
        System.out.println("Harga Kacamata :34, 26, 44");
        System.out.println("Harga baju :21, 39, 33");
        System.out.println("Output :67");
        System.out.println("Masukkan uang andi : ");
        int uangAndi = input.nextInt();
        input.nextLine();
        System.out.println("Masukkan harga kacamata = ");
        String hargaKacamata = input.nextLine();
        System.out.println("Masukkan harga Baju = ");
        String hargaBaju = input.nextLine();

        String[] jumlahHargaKacamata = hargaKacamata.split(", ");
        String[] jumlahHargaBaju = hargaBaju.split(", ");

        int[] intArray1 = new int[jumlahHargaKacamata.length];
        int[] intArray2 = new int[jumlahHargaBaju.length];

        for (int i = 0; i < intArray1.length; i++) {
            intArray1[i] = Integer.parseInt(jumlahHargaKacamata[i]);
        }
        for (int i = 0; i < intArray2.length; i++) {
            intArray2[i] = Integer.parseInt(jumlahHargaBaju[i]);
        }

        int[] totalArray = new int[intArray1.length * intArray2.length];
        int helper = 0;
        for (int i = 0; i < intArray1.length; i++) {
            for (int j = 0; j < intArray2.length; j++) {
                totalArray[helper] = intArray1[i] + intArray2[j];
                helper++;
            }
        }
//        for (int i = 0; i < totalArray.length; i++) {
//            System.out.print(totalArray[i] + " ");
//        }
        int duitDuitAndi = 0;
        Arrays.sort(totalArray);
        for (int i = 0; i < totalArray.length; i++) {
            if (totalArray[i] <= uangAndi){
                duitDuitAndi = totalArray[i];
            }
        }
        System.out.println("barang yang bisa dibeli andi dengan harga paling terdekat = " + duitDuitAndi);
    }
}
