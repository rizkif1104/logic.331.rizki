package ProblemSolving14;

import java.util.Scanner;

public class BambangKaryawanGrosir02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String jaraktoko = "0 0.5 2 3.5 5";
        int waktu = 0;
        double jarak = 0;
        double selisih = 0;
        double jaraktempuh = 0;
        double selisihJarak = 0;


        System.out.println("masukkan toko yang akan ingin dikunjungi : ");
        System.out.println("Jarak ke toko 1 = ");
        System.out.println("Jarak ke toko 2 = ");
        System.out.println("Jarak ke toko 3 = ");
        System.out.println("Jarak ke toko 4 = ");
        String jumlah = input.nextLine();
        double[] jarakKeToko = utility.ConvertStringToArrayDouble(jaraktoko);
        int[] intArray = utility.ConvertStringArrayInt(jumlah);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
            if (i == 0){
                jaraktempuh = jarakKeToko[intArray[i]];
            } else if (i == intArray.length - 1) {
                selisihJarak = jarakKeToko[intArray[i]] - jarakKeToko[intArray[i-1]];
                if (selisihJarak < 0){
                    selisihJarak *= - 1;
                }
                jaraktempuh = jaraktempuh + selisihJarak + jarakKeToko[intArray[i]];
            } else {
                selisihJarak = jarakKeToko[intArray[i]] - jarakKeToko[intArray[i-1]];
                if (selisihJarak < 0){
                    selisihJarak *= - 1;
                }
                jaraktempuh = jaraktempuh + selisihJarak;
            }
        }
        waktu = (int) (jaraktempuh * 2) + (intArray.length * 10);
        System.out.println("Waktu yang ditempuh Bambang adalah = " + waktu + " Menit");
        System.out.println("Jarak tempuh Bambang adalah = " + jaraktempuh + " KM");
    }
}