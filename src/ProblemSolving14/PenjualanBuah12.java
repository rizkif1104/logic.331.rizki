package ProblemSolving14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class PenjualanBuah12 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan items dan jumlahnya");
        System.out.println("Contoh = Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1");
        String n = input.nextLine();

        int helper = 0;

        HashMap<String, Integer> buahBuahan = new HashMap<String, Integer>();
        String [] jumlahBuah = n.split(", ");
        String[] items = new String[2];

        for (int i = 0; i < jumlahBuah.length; i++) {
            items = jumlahBuah[i].split(":");
            if (buahBuahan.get(items[0]) == null){
                buahBuahan.put(items[0],Integer.parseInt(items[1]));
            }else {
                helper = buahBuahan.get(items[0]);
                buahBuahan.put(items[0],Integer.parseInt(items[1]) + helper);
            }
        }

        String[] results = new String[buahBuahan.size()];

        int index = 0;

        for (String key : buahBuahan.keySet()){
            results[index] = key + ": " + buahBuahan.get(key);
            index++;
        }
        Arrays.sort(results);

        for (int i = 0; i < buahBuahan.size(); i++) {
            System.out.println(results[i]);        }
    }
}
