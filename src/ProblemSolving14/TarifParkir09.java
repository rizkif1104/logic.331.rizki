package ProblemSolving14;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class TarifParkir09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan tanggal & jam masuk parkir");
        System.out.println("Contoh : 28 January 2020 07:30:34");
        String jamMasuk = input.nextLine().toLowerCase();
        System.out.println("Masukkan tanggal & jam keluar parkir");
        String jamKeluar = input.nextLine().toLowerCase();

        Date masuk = null;
        Date keluar = null;

        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            masuk = dateFormat.parse(jamMasuk);
        } catch (ParseException e) {
            System.out.println("Format waktu yang diinputkan salah");
        }
        try {
            keluar = dateFormat.parse(jamKeluar);
        } catch (ParseException e) {
            System.out.println("Format waktu yang diinputkan salah");
        }

        long bedaWaktu = keluar.getTime() - masuk.getTime();
        long jedaBeda = bedaWaktu / (1000 * 60 * 60);
        System.out.println("Lama anda parkir " + jedaBeda + "jam");

        int hargaParkir = 0;
        int hari = 0;

        if (jedaBeda <= 8)
        {
            hargaParkir = (int) jedaBeda * 1000;
            System.out.println("Harga parkir " + hargaParkir);
        } else if (jedaBeda > 8 && jedaBeda <=24)
        {
            hargaParkir = 8000;
            System.out.println("harga parkir " + hargaParkir);
        } else {
            hari = (int) jedaBeda / 24;
            hargaParkir = hari * 15000;
            System.out.println("Harga parkir " + hargaParkir);
        }
    }
}
