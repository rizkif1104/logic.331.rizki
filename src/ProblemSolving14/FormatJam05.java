package ProblemSolving14;

import java.util.Scanner;

public class FormatJam05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jam : ");
        System.out.println("Contoh : 12:35 AM");
        String jam = input.nextLine().toUpperCase();

        String jamString = jam.substring(0,2);
        int jamInt = Integer.parseInt(jamString);

        if (jam.contains("AM")){
            if (jamInt == 12)
            {
                jamInt = jamInt - 12;
                String convert = Integer.toString(jamInt);
                jam = jam.replace(jamString,convert);
                jam = jam.replace("AM", "");
                System.out.println("0" + jam);
            }
            else if (jamInt < 12)
            {
                jam = jam.replace("AM", "");
                System.out.println(jam);
            }
            else
            {
                System.out.println("input jam salah");
            }
        } else if (jam.contains("PM")) {
            jamInt = jamInt + 12;
            if (jamInt < 24)
            {
                String convert = Integer.toString(jamInt);
                jam = jam.replace(jamString,convert);
                jam = jam.replace("PM", "");
                System.out.println(jam);
            } else if (jamInt == 24)
            {
                jam = jam.replace(jamString,"00");
                jam = jam.replace("PM", "");
                System.out.println(jam);
            } else
            {
                System.out.println("input jam salah");
            }

        } else
        {
            if (jamInt >= 12 && jamInt < 24)
            {
                jamInt = jamInt - 12;
                String convert = Integer.toString(jamInt);
                jam = jam.replace(jamString,convert);
                System.out.println(jam + "PM");
            }
            else if (jamInt < 12)
            {
                System.out.println(jam + "AM");
            }
            else
            {
                System.out.println("Input jam salah");
            }
        }
    }
}
