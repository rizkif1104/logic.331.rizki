package ProblemSolving14;

import java.util.Scanner;

public class Belipulsa11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan pulsa yang ingin dibeli = ");
        System.out.println("Contoh : 20000");
        int pulsa = input.nextInt();
        int point = 0;
        int pointTotal = 0;

        if (pulsa > 30000){
            pointTotal = ((pulsa - 30000) / 1000) * 2;
            pointTotal += 20;
            System.out.println("total pont yang anda dapatkan adalah " + pointTotal);
        } else if (pulsa < 30000 && pulsa > 10000) {
            pointTotal = (pulsa - 10000) / 1000;
            System.out.println("total pont yang anda dapatkan adalah " + pointTotal);
        } else {
            System.out.println("anda tidak mendapatkan pulsa");
        }
    }
}
