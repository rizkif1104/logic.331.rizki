package ProblemSolving14;

import java.util.Scanner;

public class LintasanJim07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("input pola lintasan :");
        System.out.println("Contoh");
        System.out.println("-----o----o----o-");
        String polaJalan = input.nextLine();

        System.out.println("input cara jalan :");
        System.out.println("contoh");
        System.out.println("wwwwwjwwwjwwwj");
        String caraJalan = input.nextLine().toLowerCase();

        boolean jatuh = false;
        int energi = 0;
        int helper = 0;

        for (int i = 0; i < caraJalan.length(); i++) {
            if (caraJalan.charAt(i) == 'w' && caraJalan.charAt(helper) == '-')
            {
                energi++;
                helper++;
            }
            else if (caraJalan.charAt(i) == 'w' && caraJalan.charAt(helper) == 'o')
            {
                jatuh = true;
                break;
            } else if (caraJalan.charAt(i) == 'j' && energi >= 2)
            {
                helper += 2;
                if (caraJalan.charAt(helper - 1) == 'o')
                {
                    jatuh = true;
                    break;
                }
                else
                {
                    energi -= 2;
                }
            }
            else if (caraJalan.charAt(i) == 'j' && caraJalan.charAt(helper) == 'o' && energi < 2)
            {
                jatuh = true;
                break;
            }
        }
        if (jatuh)
        {
            System.out.println("jim matek");
            System.out.println("mampos kau jim");
        }
        if (!jatuh)
        {
            System.out.println("energi jim tersisa = " + energi);
        }
    }
}
