package BangunDatar;

import java.util.Scanner;

public class PersegiPanjang {

    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas()
    {
        //luas persegi panjang
        System.out.println("masukkan panjang (cm)");
        panjang = input.nextInt();

        System.out.println("masukkan lebar (cm)");
        lebar = input.nextInt();

        int luas = panjang * lebar;

        System.out.println("Luas persegi panjang = " + luas);
    }
    public static void Keliling()
    {
        System.out.println("masukkan panjang (cm)");
        panjang = input.nextInt();

        System.out.println("masukkan lebar (cm)");
        lebar = input.nextInt();

        int keliling = (2 * panjang) + (2 * lebar);

        System.out.println("Keliling persegi panjang = " + keliling);
    }
}
