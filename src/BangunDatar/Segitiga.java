package BangunDatar;

import java.util.Scanner;

public class Segitiga {

    private static Scanner input = new Scanner(System.in);

    private static int alas;
    private static int tinggi;
    private static int bidang;

    public static void Luas()
    {
        // luas & keliling segitiga
        System.out.println("masukan sisi (cm)");
        alas = input.nextInt();

        System.out.println("masukan tinggi (cm)");
        tinggi = input.nextInt();

        System.out.println("bidang (cm)");
        bidang = input.nextInt();

        System.out.println("Luas segitiga = " + (0.5 * (alas * tinggi)));

    }
    public static void Keliling()
    {
        System.out.println("masukan sisi (cm)");
        alas = input.nextInt();

        System.out.println("masukan tinggi (cm)");
        tinggi = input.nextInt();

        System.out.println("bidang (cm)");
        bidang = input.nextInt();

        System.out.println("Keliling segitiga = " + (alas + tinggi + bidang) );

    }
}
