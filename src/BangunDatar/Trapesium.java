package BangunDatar;

import java.util.Scanner;

public class Trapesium {

    private static Scanner input = new Scanner(System.in);
    private static int alasa;
    private static int alasb;
    private static int tinggi;
    private static int bidang;

    public static void Luas()
    {
        // luas & keliling trapesium
        System.out.println("masukan Alas atas (cm)");
        alasa = input.nextInt();

        System.out.println("masukan Alas bawah (cm)");
        alasb = input.nextInt();

        System.out.println("masukan Tinggi (cm)");
        tinggi = input.nextInt();

        System.out.println("masukan Bidang (cm)");
        bidang = input.nextInt();

        System.out.println("Luas trapesium = " + (0.5 * (alasa + alasb) * tinggi));
    }
    public static void Keliling()
    {
        System.out.println("masukan Alas atas (cm)");
        alasa = input.nextInt();

        System.out.println("masukan Alas bawah (cm)");
        alasb = input.nextInt();

        System.out.println("masukan Tinggi (cm)");
        tinggi = input.nextInt();

        System.out.println("masukan Bidang (cm)");
        bidang = input.nextInt();

        System.out.println("Keliling trapesium = " + (alasa + alasb + bidang + tinggi));
    }
}
