package BangunDatar;

// string untuk karakter
// int untuk nomer / bilangan
// double untuk bilangan desimal
// boolean untuk true atau false
// system out print untuk menampilkan hasil
// jika variabel berwarna abu abu = belum dipanggil / digunakan

import java.util.Scanner;

public class Main {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        boolean flag = true;
        String answer = "y";

        while (flag)
        {
            System.out.println("Pilih bangun datar");
            System.out.println("1. Persegi panjang");
            System.out.println("2. Segitiga");
            System.out.println("3. Trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");

            int pilihan1 = input.nextInt();
            int pilihan2;

            String prompt = "Pilihan     1.Luas      2.Keliling";

            switch (pilihan1) {
                case 1:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        PersegiPanjang.Luas();
                    } else if (pilihan2 == 2) {
                        PersegiPanjang.Keliling();
                    } else  {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 2:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Segitiga.Luas();
                    } else if (pilihan2 == 2) {
                        Segitiga.Keliling();
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 3:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Trapesium.Luas();
                    } else if (pilihan2 == 2) {
                        Trapesium.Keliling();
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 4:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Lingkaran.Luas();
                    } else if (pilihan2 == 2) {
                        Lingkaran.Keliling();
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 5:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Persegi.Luas();
                    } else if (pilihan2 == 2) {
                        Persegi.Keliling();
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                default:
                    System.out.println("Pilihan tidak tersedia");
        }

            System.out.println("Try again? y/n");
            input.nextLine(); //skip
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y"))
            {
                flag = false;
            }


        }
    }
}