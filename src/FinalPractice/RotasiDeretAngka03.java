package FinalPractice;

import java.util.Scanner;

public class RotasiDeretAngka03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka");
        System.out.println("Contoh : 7 3 9 9 2");
        String data = input.nextLine();

        String[] rotasiArray = data.split(" ");
        System.out.println("Tentukan berapa kali rotasi : ");
        int rotasi = input.nextInt();

        int helper = 0;


        for (int i = 0; i < rotasi; i++) {
            System.out.println("Rotasi " + (i + 1) + " : ");
            helper = Integer.parseInt(rotasiArray[0]);
            for (int j = 0; j < rotasiArray.length; j++) {
                if (j == rotasiArray.length - 1)
                {
                    rotasiArray[j] = String.valueOf(helper);
                    System.out.print(rotasiArray[j] + " ");
                }
                else
                {
                    rotasiArray[j] = rotasiArray[j + 1];
                    System.out.print(rotasiArray[j] + " ");
                }
            }
            System.out.println();
        }
    }
}
