package FinalPractice;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                StikEsLoli01.Resolve(); //done
                break;
            case 2:
                Palindrome02.Resolve(); //done
                break;
            case 3:
                RotasiDeretAngka03.Resolve(); //done
                break;
            case 4:
                RestoranIkan04.Resolve(); //not done
                break;
            case  5:
                formatJam05.Resolve(); //done
                break;
            case 6:

                break;
            case 7:
                meanMedianModus07.Resolve(); //done
                break;
            case 8:

                break;
            case 9:
                SudutJam09.Resolve(); //done
                break;
            case 10:
                takaranCupcake10.Resolve(); //done
                break;
            default:
        }
    }
}