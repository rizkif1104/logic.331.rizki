package FinalPractice;

import java.util.Scanner;

public class Palindrome02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program mengecek palindrome atau tidak");
        System.out.println("Masukkan kalimat = ");
        System.out.println("Contoh");
        System.out.println("Malam");
        String inputData = input.nextLine().toLowerCase();

        StringBuilder reverseData = new StringBuilder();
        reverseData.append(inputData);
        reverseData.reverse();
        String reverseDataString = reverseData.toString();

        if (!reverseDataString.equals(inputData))
        {
            System.out.println("Bukan Palindrome");
        }
        else
        {
            System.out.println("Palindrome");
        }
    }
}
