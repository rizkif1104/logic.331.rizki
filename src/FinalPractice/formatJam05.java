package FinalPractice;

import com.sun.source.tree.IfTree;

import java.util.Scanner;

public class formatJam05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("input jam yang ingin dimasukkan dalam format AM / PM");
        System.out.println("Contoh 03:40:44 PM");
        String jam = input.nextLine();

        String jamString = jam.substring(0,2);
        int jamInt = Integer.parseInt(jamString);

        if (jam.contains("AM"))
        {
            if (jamInt == 12)
            {
                jamInt = jamInt - 12;
                String convert = Integer.toString(jamInt);
                jam = jam.replace(jamString,convert);
                jam = jam.replace("AM", "");
                System.out.println("0" + jam);
            } else if (jamInt < 12)
            {
                jam = jam.replace("AM", "");
                System.out.println(jam);
            }
            else
            {
                System.out.println("Input yang anda masukkan salah");
            }
        } else if (jam.contains("PM"))
        {
            jamInt = jamInt + 12;
            if (jamInt < 24)
            {
                String convert = Integer.toString(jamInt);
                jam = jam.replace(jamString,convert);
                jam = jam.replace("PM", "");
                System.out.println(jam);
            } else if (jamInt == 24)
            {
                jam = jam.replace(jamString,"00");
                jam = jam.replace("PM", "");
                System.out.println(jam);
            }
            else
            {
                System.out.println("Input yang anda masukkan salah");
            }
        }
    }
}
