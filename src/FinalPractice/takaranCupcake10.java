package FinalPractice;

import java.util.Scanner;

public class takaranCupcake10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan cupcake yang ingin dibuat");
        int inputCupcake = input.nextInt();

        double terigu = 8.33333333;
        double gulaPasir = 6.66666667;
        double susu = 6.66666667;

        int bahanTerigu = (int) (inputCupcake * terigu);
        int bahanGula = (int) (inputCupcake * gulaPasir);
        int bahanSusu = (int) (inputCupcake * susu);

        System.out.println("jumlah bahan yang diperlukan untuk membuat " + inputCupcake + " cupcake adalah = " + bahanTerigu + " KG Terigu, " + bahanGula + " KG gula, " + bahanSusu + " Ml susu");
    }
}
