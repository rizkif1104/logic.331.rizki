package Warmup;

import inputPlural.utility;

import java.util.Scanner;

public class MiniMaxSum07 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan deret angka : ");
        String inputan = input.nextLine();

        int[] nilai = utility.ConvertStringArrayInt(inputan);
        int panjang = nilai.length;
        int terbesar = 0;
        int nilaiTerbesar = 0;
        int terkecil = 0;
        int nilaiTerkecil = 0;

        for (int i = 0; i < panjang; i++) {
            if (i == 0){
                terbesar = nilai[0];
                terkecil = nilai[0];
            }else {
                if (terbesar < nilai[i]){
                    terbesar = nilai[i];
                }
                if (terkecil > nilai[i]){
                    terkecil = nilai[i];
                }
            }
        }

        for (int i = 0; i < panjang; i++) {
            if (nilai[i] < terbesar){
                nilaiTerkecil += nilai[i];
            }
            if (nilai[i] > terkecil){
                nilaiTerbesar += nilai[i];
            }
        }

        System.out.println("Nilai Terbesar = " + nilaiTerbesar);
        System.out.println("Nilai Terkecil = " + nilaiTerkecil);
    }
}
