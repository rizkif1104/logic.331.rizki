package Warmup;

import java.util.Scanner;

public class PlusMinus05 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan panjang array: ");
        int n = input.nextInt();
        int[] arr = new int[n];

        System.out.println("Masukkan isi array:");

        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

        utility.kalkulus(arr);
    }
}
