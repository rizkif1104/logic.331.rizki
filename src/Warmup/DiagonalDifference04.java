package Warmup;

import java.util.Scanner;

//Sample Input
//
//        3
//        11  2  4
//        4   5  6
//        10  8 -12
//        Sample Output
//
//        15

public class DiagonalDifference04 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan ukuran matriks ..X : ");
        int n = input.nextInt();

        int[][] matrix = new int[n][n];

        System.out.println("Masukkan isi matriks:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = input.nextInt();
            }
        }

        int diaonal1 = 0;
        int diagonal2 = 0;

        for (int i = 0; i < n; i++) {
            diaonal1 += matrix[i][i];
            diagonal2 += matrix[i][n - 1 - i];
        }

        int output = Math.abs(diaonal1 - diagonal2);

        System.out.println("Perbedaan antara diagonal utama dan diagonal sekunder: " + output);
    }
}
