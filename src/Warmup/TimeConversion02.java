package Warmup;

import java.util.Scanner;
import java.util.Date;
import java.text.SimpleDateFormat;


public class TimeConversion02 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan waktu dalam format 12 jam (hh:mm:ss AM/PM): ");
        String waktu12Jam = input.nextLine();

        SimpleDateFormat dateFormat12 = new SimpleDateFormat("hh:mm:ss a");
        SimpleDateFormat dateFormat24 = new SimpleDateFormat("HH:mm:ss");

        try {
            Date date = dateFormat12.parse(waktu12Jam);
            String waktu24Jam = dateFormat24.format(date);
            System.out.println("Waktu dalam format 24 jam: " + waktu24Jam);
        } catch (Exception e) {
            System.out.println("Format waktu tidak valid. Pastikan memasukkan waktu dengan format yang benar.");
        }
    }
}
