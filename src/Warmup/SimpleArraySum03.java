package Warmup;


import java.util.Scanner;

public class SimpleArraySum03 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Input derat angka");
        String text = input.nextLine();

        int[] intArray = utility.Convert(text);
        int length = intArray.length;

        int total = 0;


        for (int i = 0; i < length; i++) {
            total += intArray[i];

        }
        System.out.print("Total = ");
        System.out.print(total);
    }
}