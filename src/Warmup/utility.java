package Warmup;

public class utility {
    public static void PrintArrayKarakter(String[][] results)
    {
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                System.out.print(results[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void kalkulus(int[] arr) {
        int positif = 0;
        int negatif = 0;
        int zero = 0;
        int n = arr.length;

        for (int num : arr) {
            if (num > 0) {
                positif++;
            } else if (num < 0) {
                negatif++;
            } else {
                zero++;
            }
        }

        double positiveRatio = (double) positif / n;
        double negativeRatio = (double) negatif / n;
        double zeroRatio = (double) zero / n;

        System.out.println("Proporsi angka positif: " + positiveRatio);
        System.out.println("Proporsi angka negatif: " + negativeRatio);
        System.out.println("Proporsi angka nol: " + zeroRatio);
    }
    public static int[] Convert(String text)
    {
        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        return intArray;
    }
}