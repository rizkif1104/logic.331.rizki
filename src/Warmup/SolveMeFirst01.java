package Warmup;

import java.util.Scanner;

public class SolveMeFirst01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int angka1;
        int angka2;

        System.out.println("masukkan angka ke 1 : ");
        angka1 = input.nextInt();
        System.out.println("masukkan angka ke 2 : ");
        angka2 = input.nextInt();

        System.out.println("Hasil dari " + angka1 + " + " + angka2 + " adalah : " + (angka1 + angka2));
    }
}
