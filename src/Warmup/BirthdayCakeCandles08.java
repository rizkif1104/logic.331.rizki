package Warmup;

import java.util.Scanner;

public class BirthdayCakeCandles08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan jumlah lilin: ");
        int n = input.nextInt();
        int[] tinggi = new int[n];

        System.out.println("Masukkan tinggi lilin : ");
        for (int i = 0; i < n; i++) {
            tinggi[i] = input.nextInt();
        }

        int tinggiMaks = tinggi[0];
        int count = 0;

        for (int i = 0; i < n; i++) {
            if (tinggi[i] > tinggiMaks) {
                tinggiMaks = tinggi[i];
                count = 1;
            } else if (tinggi[i] == tinggiMaks) {
                count++;
            }
        }

        System.out.println("Jumlah lilin tertinggi yang dapat ditiupkan: " + count);
    }
}
