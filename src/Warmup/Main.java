package Warmup;

import java.util.Scanner;

//Complete the function solveMeFirst to compute the sum of two integers.
//        Sample Input
//
//        a = 2
//        b = 3
//        Sample Output
//
//        5
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                SolveMeFirst01.Resolve(); //done
                break;
            case 2:
                TimeConversion02.Resolve(); //done
                break;
            case 3:
                SimpleArraySum03.Resolve(); //done
                break;
            case 4:
                DiagonalDifference04.Resolve(); //done
                break;
            case 5:
                PlusMinus05.Resolve(); //done
                break;
            case 6:
                Staircase06.Resolve(); //done
                break;
            case 7:
                MiniMaxSum07.Resolve(); //done
                break;
            case 8:
                BirthdayCakeCandles08.Resolve(); // done
                break;
            case 9:
                AVeryBigSum09.Resolve(); //done
                break;
            case 10:
                CompareTheTriplets10.Resolve(); //done
                break;
            default:
        }
    }
}