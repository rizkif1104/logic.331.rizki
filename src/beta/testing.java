package beta;

import java.util.Scanner;
public class testing {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int PIN_Nasabah = 0;
        int Saldo_Nasabah = 0;
        int PIN_Salah = 0;
        boolean Blokir_Akun = false;

        // Step 1: Meminta nasabah untuk membuat PIN
        while (PIN_Nasabah == 0) {
            System.out.print("Buat PIN (minimal 6 angka): ");
            int PIN = input.nextInt();
            if (String.valueOf(PIN).length() == 6) {
                PIN_Nasabah = PIN;
            } else {
                System.out.println("PIN harus terdiri dari 6 angka. Silakan coba lagi.");
                PIN_Salah++;
            }

            if (PIN_Salah >= 3) {
                Blokir_Akun = true;
                System.out.println("Akun Anda telah terblokir. Silakan hubungi bank.");
                System.exit(0);
            }
        }

        while (!Blokir_Akun) {
            // Step 3: Menampilkan pilihan menu
            System.out.println("Pilih menu:");
            System.out.println("1. Setoran Tunai");
            System.out.println("2. Transfer");
            System.out.println("3. Keluar");
            System.out.print("Pilihan Anda: ");
            int pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    // Step 4.1: Setoran Tunai
                    System.out.print("Masukkan nominal uang yang akan disetor: ");
                    int nominalSetoran = input.nextInt();
                    if (nominalSetoran > 25000000) {
                        System.out.println("Limit setoran harian maksimal adalah 25 juta. Silakan coba lagi.");
                    } else {
                        System.out.print("Are you sure? (y/n): ");
                        String konfirmasi = input.next();
                        if (konfirmasi.equalsIgnoreCase("y")) {
                            Saldo_Nasabah += nominalSetoran;
                            System.out.println("Transaksi berhasil.");
                            System.out.println("Sisa saldo: " + Saldo_Nasabah);
                        }
                    }
                    break;

                case 2:
                    // Step 4.2: Transfer
                    System.out.println("Pilih jenis transfer:");
                    System.out.println("1. Antar Rekening");
                    System.out.println("2. Antar Bank");
                    System.out.print("Pilihan Anda: ");
                    int jenisTransfer = input.nextInt();

                    if (jenisTransfer == 1) {
                        // Antar Rekening
                        System.out.print("Masukkan nomor rekening penerima (7 angka): ");
                        int nomorRekening = input.nextInt();
                        // Validasi nomor rekening (7 angka)
                        if (String.valueOf(nomorRekening).length() != 7) {
                            System.out.println("Nomor rekening penerima harus terdiri dari 7 angka. Silakan coba lagi.");
                            break;
                        }
                    } else if (jenisTransfer == 2) {
                        // Antar Bank
                        System.out.print("Masukkan kode bank awal (3 angka): ");
                        int kodeBank = input.nextInt();
                        System.out.print("Masukkan nomor rekening penerima (7 angka): ");
                        int nomorRekening = input.nextInt();
                        // Validasi kode bank dan nomor rekening (10 angka)
                        if (String.valueOf(kodeBank).length() != 3 || String.valueOf(nomorRekening).length() != 7) {
                            System.out.println("Kode bank harus terdiri dari 3 angka dan nomor rekening penerima harus terdiri dari 7 angka. Silakan coba lagi.");
                            break;
                        }
                    } else {
                        System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                        break;
                    }

                    System.out.print("Masukkan jumlah uang yang akan ditransfer: ");
                    int jumlahTransfer = input.nextInt();

                    if (jumlahTransfer > Saldo_Nasabah) {
                        System.out.println("Saldo tidak mencukupi. Transfer gagal.");
                    } else {
                        Saldo_Nasabah -= jumlahTransfer;
                        System.out.println("Transaksi berhasil.");
                        System.out.println("Sisa saldo: " + Saldo_Nasabah);
                    }
                    break;

                case 3:
                    // Keluar
                    System.out.println("Terima kasih. Selamat tinggal.");
                    System.exit(0);
                    break;

                default:
                    System.out.println("Pilihan tidak valid. Silakan coba lagi.");
            }
        }
    }
}
