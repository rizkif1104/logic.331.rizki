package Array2D;

import java.util.Scanner;

public class Soal6 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int baris1 = 0;
        int baris2 = 1;
        int baris3 = 1;

        System.out.println("Input nilai N ");
        int n = input.nextInt();

        int[][] array2d = new int[3][n];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    array2d[i][j] = baris1;
                    baris1 = baris1 + 1;
                } else if (i == 1) {
                    array2d[i][j] = baris2;
                    baris2 = baris2 * 7;
                } else if (i == 2) {
                    baris3 = array2d[0][j] + array2d[1][j];
                    array2d[i][j] = baris3;
                }
            }
        }
        utility.PrintArray2D(array2d);
    }
}
