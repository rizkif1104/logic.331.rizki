package Array2D;

import java.util.Scanner;

public class Soal4 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int baris1 = 0;
        int baris2 = 1;
        int baris3 = 5;

        System.out.println("Masukkan nilai N");
        int n = input.nextInt();
        int[][] array2d = new int[2][n];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    array2d[i][j] = baris1;
                    baris1 = baris1 + 1;
                } else if (i == 1)
                {
                    if (j % 2 == 0){
                        array2d[i][j] = baris2;
                        baris2 = baris2 + 1;
                    } else {
                        array2d[i][j] = baris3;
                        baris3 += 5;
                    }
                }
            }
        }
        utility.PrintArray2D(array2d);
    }
}
