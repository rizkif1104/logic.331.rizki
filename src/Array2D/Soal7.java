package Array2D;

import java.util.Scanner;

public class Soal7 {
    public static void Resolve(){
        Scanner scanner = new Scanner(System.in);

        int baris = 3;
        int tanda = 0;

        System.out.println("Input N: ");
        int kolom = scanner.nextInt();

        int[][] results = new int[baris][kolom];

        int value = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < kolom; j++) {
                if((tanda) % 3 == 0){
                    value *= -1;
                    results[i][j] = value;
                    value *= -1;
                    value++;
                    tanda++;
                } else {
                    results[i][j] = value;
                    value += 1;
                    tanda++;
                }
            }
        }
        utility.PrintArray2D(results);
    }
}
