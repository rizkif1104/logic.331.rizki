package Array2D;

import java.util.Scanner;

public class Soal5 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int baris = 3;
        int nilai = 0;

        System.out.println("input N");
        int n = input.nextInt();

        int array2d[][] = new int[3][n];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                array2d[i][j] = nilai;
                nilai = nilai + 1;
            }
        }
        utility.PrintArray2D(array2d);
    }
}
