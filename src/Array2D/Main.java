package Array2D;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (0 - 12)");
        pilihan = input.nextInt();

        while (pilihan < 0 || pilihan > 12)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 0:
                contoh.Resolve(); //latihan
                break;
            case 1:
                Soal1.Resolve(); //done
                break;
            case 2:
                Soal2.Resolve(); //done
                break;
            case 3:
                Soal3.Resolve(); //done
                break;
            case 4:
                Soal4.Resolve(); //done
                break;
            case 5:
                Soal5.Resolve(); //done
                break;
            case 6:
                Soal6.Resolve(); //done
                break;
            case 7:
                Soal7.Resolve(); //done
                break;
            case 8:
                Soal8.Resolve(); //done
                break;
            case 9:
                Soal9.Resolve(); //done
                break;
            case 10:
                Soal10.Resolve(); //done
                break;
            case 11:
                Soal11.Resolve(); //done
                break;
            case 12:

                break;
            default:
        }
    }
}