package Array2D;

import java.util.Scanner;

public class Soal2 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int baris1 = 0;
        int baris2 = 1;

        System.out.println("Input nilai n");
        int n = input.nextInt();

        int[][] results = new int[2][n];

        for (int i = 0; i < 2; i++){
            for (int j = 0; j < n; j++){
                if (i == 0)
                {
                    results[i][j] = baris1;
                    baris1 += 1;
                }
                else if (i == 1)
                {
                    if((j + 1) % 3 == 0){
                        baris2 *= -1;
                        results[i][j] = baris2;
                        baris2 *= -3;
                    } else {
                        results[i][j] = baris2;
                        baris2 *= 3;
                    }
                }
            }
        }
        utility.PrintArray2D(results);
    }
}
