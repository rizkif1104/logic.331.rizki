package Array2D;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Panjang Colom (n) = ");
        int n = input.nextInt();
        int data = 0;
        String simbol = "*";

        String[][] result = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n-(i+1)); j++) {
                result[i][data] = " ";
                data += 1;
            }
            for (int k = 0; k < (i+1); k++) {
                result[i][data] = simbol;
                data += 1;
            }
            data = 0;
        }
        utility.PrintArrayKarakter(result);
    }
}
